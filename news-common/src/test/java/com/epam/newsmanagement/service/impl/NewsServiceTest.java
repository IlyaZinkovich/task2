package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.domain.Tag;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@ContextConfiguration(locations = {"classpath:spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceTest {

    @Mock
    private NewsDAO newsDAO;

    @InjectMocks
    private NewsServiceImpl newsService;

    private News testNews;
    private Long testTagId;
    private Author testAuthor;
    private List<Long> testTagsId;
    private final int testPage = 1;
    private final int testPageSize = 3;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        testNews = new News(1l, "short", "full", "title", new Date(), new Date());
        testAuthor = new Author(1l, "author", null);
        testTagId = 1L;
        testTagsId = new LinkedList<>();
        testTagsId.add(1l);
        testTagsId.add(2l);
        testTagsId.add(3l);
        List<Tag> testTags = new LinkedList<>();
        testTags.add(new Tag(1l, "first"));
        testTags.add(new Tag(2l, "second"));
        testTags.add(new Tag(3l, "third"));
        List<News> testNewsList = new LinkedList<>();
        testNewsList.add(testNews);
    }

    @Test
    public void addNewsSucceed() throws Exception {
        when(newsDAO.insert(testNews)).thenReturn(1l);
        long generatedId = newsService.addNews(testNews);
        assertThat(generatedId, greaterThan(0l));
        verify(newsDAO).insert(testNews);
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void editNewsSucceed() throws Exception {
        testNews.setTitle("testTitle");
        newsService.editNews(testNews);
        ArgumentCaptor<News> newsCaptor = ArgumentCaptor.forClass(News.class);
        verify(newsDAO).update(newsCaptor.capture());
        News updatedNews = newsCaptor.getValue();
        assertThat(updatedNews, is(testNews));
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void deleteNewsSucceed() throws Exception {
        newsService.deleteNews(testNews.getId());
        verify(newsDAO).delete(testNews.getId());
        verify(newsDAO).deleteNewsAuthor(testNews.getId());
        verify(newsDAO).deleteNewsTags(testNews.getId());
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void findByIdSucceed() throws Exception {
        when(newsDAO.findById(testNews.getId())).thenReturn(testNews);
        News foundNews = newsService.findById(testNews.getId());
        assertThat(foundNews, is(testNews));
        verify(newsDAO).findById(testNews.getId());
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void addNewsAuthorSucceed() throws Exception {
        newsService.addNewsAuthor(testNews.getId(), testAuthor.getId());
        verify(newsDAO).insertNewsAuthor(testNews.getId(), testAuthor.getId());
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void addNewsTagSucceed() throws Exception {
        newsService.addNewsTag(testNews.getId(), testTagId);
        verify(newsDAO).insertNewsTag(testNews.getId(), testTagId);
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void addNewsTagsSucceed() throws Exception {
        newsService.addNewsTags(testNews.getId(), testTagsId);
        verify(newsDAO).insertNewsTags(testNews.getId(), testTagsId);
        verifyNoMoreInteractions(newsDAO);
    }

    @Test
    public void findNewsSucceed() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        when(newsDAO.findNews(sc)).thenReturn(new LinkedList<>());
        newsService.findNews(sc);
        verify(newsDAO).findNews(sc);
        verifyNoMoreInteractions(newsDAO);
    }

}
