package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-test.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dbunitxml/news-data.xml")
public class NewsDAOTest {

    private static Logger logger = Logger.getLogger(NewsDAOTest.class);

    @Autowired
    private NewsDAO newsDAO;

    @Test
    public void findByIdSucceed() throws Exception {
        News news = newsDAO.findById(1L);
        assertThat(news, notNullValue());
        assertThat(news.getId(), is(1L));
    }

    @Test
    public void insertNewsSucceed() throws Exception {
        News news = new News(null, "13", "13", "13", new Date(), new Date());
        Long newsId = newsDAO.insert(news);
        assertNotNull(newsId);
        assertThat(newsId, greaterThanOrEqualTo(1L));
    }

    @Test
    public void updateNewsSucceed() throws Exception {
        News news = new News(1L, "13", "13", "13", new Date(), DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH));
        newsDAO.update(news);
        News updatedNews = newsDAO.findById(1L);
        assertEquals(updatedNews, news);
    }

    @Test
    public void deleteNewsWithDeletingConstraintsSucceed() throws Exception {
        Long newsIdToDelete = 12L;
        newsDAO.deleteNewsAuthor(newsIdToDelete);
        newsDAO.deleteNewsTags(newsIdToDelete);
        newsDAO.deleteNewsComments(newsIdToDelete);
        newsDAO.delete(newsIdToDelete);
        News news = newsDAO.findById(newsIdToDelete);
        assertNull(news);
    }

    @Test(expected = DAOException.class)
    public void deleteNewsWithoutDeletingConstraintsExpectException() throws Exception {
        newsDAO.delete(12L);
    }

    @Test
    public void findAllSucceed() throws Exception {
        List<News> foundNews = newsDAO.findNews(new SearchCriteria());
        assertNotNull(foundNews);
        assertEquals(foundNews.size(), 13);
    }

    @Test
    public void findByAuthorSucceed() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(2L);
        List<News> foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        List<Long> foundNewsIdList = foundNewsList.stream().map(News::getId)
                .sorted().collect(Collectors.toList());
        assertEquals(foundNewsIdList, Arrays.asList(3L, 4L, 5L));
    }

    @Test
    public void findByAuthorReturnsEmptyListIfNewsNotFound() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(13L);
        List foundNewsList = newsDAO.findNews(sc);
        assertEquals(foundNewsList, Collections.EMPTY_LIST);
    }

    @Test
    public void findByTagsSucceed() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setTagsId(Arrays.asList(3L, 4L, 5L));
        List<News> foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        List<Long> foundNewsIdList = foundNewsList.stream().map(News::getId)
                .sorted().collect(Collectors.toList());
        assertEquals(foundNewsIdList, Arrays.asList(1L, 2L, 3L, 4L, 6L, 7L, 9L, 10L));
    }

    @Test
    public void findByTagsReturnsEmptyListIfNewsNotFound() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(13L);
        List foundNewsList = newsDAO.findNews(sc);
        assertEquals(foundNewsList, Collections.EMPTY_LIST);
    }

    @Test
    public void findByAuthorAndTagsSucceed() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(2L);
        sc.setTagsId(Arrays.asList(1L, 4L, 6L));
        List<News> foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        List<Long> foundNewsIdList = foundNewsList.stream().map(News::getId)
                .sorted().collect(Collectors.toList());
        assertEquals(foundNewsIdList, Arrays.asList(3L, 4L));
    }

    @Test
    public void findByAuthorAndTagsSucceedReturnsEmptyListIfNewsNotFound() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(2L);
        sc.setTagsId(Arrays.asList(1L, 2L));
        List foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        assertEquals(foundNewsList, Collections.EMPTY_LIST);
    }

    @Test
    public void findByPageOfPageSizeSucceed() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setPage(2L);
        sc.setPageSize(4L);
        List<News> newsList = newsDAO.findNews(sc);
        assertNotNull(newsList);
        assertEquals(newsList.size(), 4);
        assertEquals(newsList.get(0).getId(), Long.valueOf(6L));
    }

    @Test
    public void findByPageOfPageSizeReturnsEmptyListIfNewsNotFound() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setPage(10L);
        sc.setPageSize(5L);
        List newsList = newsDAO.findNews(sc);
        assertNotNull(newsList);
        assertEquals(newsList, Collections.EMPTY_LIST);
    }

    @Test
    public void findByAuthorAndTagsForPageOfSizeSucceed() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(2L);
        sc.setTagsId(Arrays.asList(1L, 4L, 6L));
        sc.setPage(1L);
        sc.setPageSize(4L);
        List<News> foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        List<Long> foundNewsIdList = foundNewsList.stream().map(News::getId)
                .sorted().collect(Collectors.toList());
        assertEquals(foundNewsIdList, Arrays.asList(3L, 4L));
    }


    @Test
    public void findByAuthorAndTagsForPageOfSizeReturnsEmptyListIfNewsNotFound() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(2L);
        sc.setTagsId(Arrays.asList(1L, 4L, 6L));
        sc.setPage(2L);
        sc.setPageSize(4L);
        List foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        assertEquals(foundNewsList, Collections.EMPTY_LIST);
    }

    @Test
    public void getNewsCountSucceed() throws Exception {
        Long newsCount = newsDAO.getNewsCount(new SearchCriteria());
        assertNotNull(newsCount);
        assertEquals(newsCount, Long.valueOf(13L));
    }

    @Test
    public void findPreviousAndNextNewsSucceed() throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<Long> previousAndNextNews = newsDAO.findPreviousAndNextNews(2L, searchCriteria)
                .stream().sorted().collect(Collectors.toList());
        assertNotNull(previousAndNextNews);
        assertEquals(previousAndNextNews, Arrays.asList(3L, 6L));
    }

    @Test
    public void findPreviousAndNextNewsReturnZeroAsPreviousIfNewsIsFirst() throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<News> allNewsList = newsDAO.findNews(searchCriteria);
        List<Long> previousAndNextNews = newsDAO.findPreviousAndNextNews(allNewsList.get(0).getId(), searchCriteria)
                .stream().collect(Collectors.toList());
        assertNotNull(previousAndNextNews);
        assertEquals(previousAndNextNews, Arrays.asList(0L, allNewsList.get(1).getId()));
    }

    @Test
    public void findPreviousAndNextNewsReturnZeroAsPreviousIfNewsIsLast() throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<News> allNewsList = newsDAO.findNews(searchCriteria);
        List<Long> previousAndNextNews = newsDAO.findPreviousAndNextNews(allNewsList.get(allNewsList.size() - 1).getId(),
                searchCriteria).stream().collect(Collectors.toList());
        assertNotNull(previousAndNextNews);
        assertEquals(previousAndNextNews, Arrays.asList(allNewsList.get(allNewsList.size() - 2).getId(), 0L));
    }

}
