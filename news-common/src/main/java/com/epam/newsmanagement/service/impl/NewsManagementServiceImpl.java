package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.domain.*;
import com.epam.newsmanagement.service.*;
import com.epam.newsmanagement.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Transactional(rollbackFor = ServiceException.class)
public class NewsManagementServiceImpl implements NewsManagementService {

    private static Logger logger = Logger.getLogger(NewsManagementServiceImpl.class);

    private NewsService newsService;
    private AuthorService authorService;
    private TagService tagService;
    private CommentService commentService;

    public NewsManagementServiceImpl() {
    }

    public NewsManagementServiceImpl(NewsService newsService, AuthorService authorService, TagService tagService, CommentService commentService) {
        this.newsService = newsService;
        this.authorService = authorService;
        this.tagService = tagService;
        this.commentService = commentService;
    }

    @Override
    public void addNewsAuthor(long newsId, Author author) throws ServiceException {
        if (newsService.findById(newsId) != null) {
            long authorId = authorService.addAuthor(author);
            newsService.addNewsAuthor(newsId, authorId);
        }
    }

    @Override
    public void addNewsTags(long newsId, List<Tag> tags) throws ServiceException {
        if (newsService.findById(newsId) != null) {
            List<Long> tagIdList = tagService.addTags(tags);
            newsService.addNewsTags(newsId, tagIdList);
        }
    }

    @Override
    public void addNewsTag(long newsId, Tag tag) throws ServiceException {
        if (newsService.findById(newsId) != null) {
            long tagId = tagService.addTag(tag);
            newsService.addNewsTag(newsId, tagId);
        }
    }

    @Override
    public void deleteNewsWithComments(long newsId) throws ServiceException {
        List<Comment> commentsToDelete = commentService.findByNewsId(newsId);
        for (Comment comment : commentsToDelete)
            commentService.deleteComment(comment.getId());
        newsService.deleteNews(newsId);
    }

    @Override
    public void deleteNewsWithComments(List<Long> newsId) throws ServiceException {
        if (newsId != null) {
            newsId.forEach(this::deleteNewsWithComments);
        }
    }

    @Override
    public void addNewsDTO(News news, Author author, List<Tag> tags) throws ServiceException {
        long newsId = newsService.addNews(news);
        long authorId = authorService.addAuthor(author);
        newsService.addNewsAuthor(newsId, authorId);
        List<Long> tagIdList =  tagService.addTags(tags);
        newsService.addNewsTags(newsId, tagIdList);
    }

    @Override
    public void addNewsDTO(NewsDTO news) throws ServiceException {
        addNewsDTO(news.getNews(), news.getAuthor(), news.getTags());
    }

    @Override
    public NewsDTO findNewsDTOById(long newsId) throws ServiceException {
        NewsDTO complexNews = new NewsDTO();
        News news = newsService.findById(newsId);
        Author author = authorService.findByNewsId(newsId);
        List<Tag> tags = tagService.findByNewsId(newsId);
        List<Comment> comments = commentService.findByNewsId(newsId);
        complexNews.setNews(news);
        complexNews.setAuthor(author);
        complexNews.setTags(tags);
        complexNews.setComments(comments);
        return complexNews;
    }


    @Override
    public List<NewsDTO> findNewsDTO(SearchCriteria searchCriteria) throws ServiceException {
        List<News> news = newsService.findNews(searchCriteria);
        List<NewsDTO> newsDTOList = new LinkedList<>();
        for (News n : news) {
            Author author = authorService.findByNewsId(n.getId());
            List<Tag> tags = tagService.findByNewsId(n.getId());
            List<Comment> comments = commentService.findByNewsId(n.getId());
            NewsDTO newsDTO = new NewsDTO(n, author, tags, comments);
            newsDTOList.add(newsDTO);
        }
        return newsDTOList;
    }


    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void editNews(News news, Long authorId, List<Long> tagsId) throws ServiceException {
        newsService.deleteNewsAuthor(news.getId());
        newsService.deleteNewsTag(news.getId());
        newsService.editNews(news);
        newsService.addNewsAuthor(news.getId(), authorId);
        newsService.addNewsTags(news.getId(), tagsId);
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public Long addNews(News news, Long authorId, List<Long> tagsId) throws ServiceException {
        Long newsId = newsService.addNews(news);
        newsService.addNewsAuthor(newsId, authorId);
        newsService.addNewsTags(newsId, tagsId);
        return newsId;
    }

    @Override
    public void saveNews(News news, Long authorId, List<Long> tagsId) throws ServiceException {
        if (news.getId() != null) {
            editNews(news, authorId, tagsId);
        } else {
            news.setModificationDate(new Date());
            news.setId(addNews(news, authorId, tagsId));
        }
    }

    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }

    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }

    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }
}
