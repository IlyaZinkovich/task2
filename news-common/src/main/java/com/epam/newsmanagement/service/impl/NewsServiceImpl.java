package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class NewsServiceImpl implements NewsService {

    private static final Logger logger = Logger.getLogger(NewsServiceImpl.class);

    private NewsDAO newsDAO;

    public NewsServiceImpl() {
    }

    public NewsServiceImpl(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    @Override
    public long addNews(News news) throws ServiceException {
        try {
            return newsDAO.insert(news);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void editNews(News news) throws ServiceException {
        try {
            newsDAO.update(news);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void deleteNews(long newsId) throws ServiceException {
        try {
            newsDAO.deleteNewsTags(newsId);
            newsDAO.deleteNewsAuthor(newsId);
            newsDAO.delete(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNewsList(List<Long> newsId) throws ServiceException {
        try {
            for (Long id : newsId) {
                newsDAO.delete(id);
            }
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public News findById(long newsId) throws ServiceException {
        try {
            return newsDAO.findById(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void addNewsAuthor(long newsId, long authorId) throws ServiceException {
        try {
            newsDAO.insertNewsAuthor(newsId, authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void addNewsTag(long newsId, long tagId) throws ServiceException {
        try {
            newsDAO.insertNewsTag(newsId, tagId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void addNewsTags(long newsId, List<Long> tagIdList) throws ServiceException {
        try {
            if (tagIdList != null) {
                newsDAO.insertNewsTags(newsId, tagIdList);
            }
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> findNews(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.findNews(searchCriteria);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long getNewsCount(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.getNewsCount(searchCriteria);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Long> findPreviousAndNextNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.findPreviousAndNextNews(newsId, searchCriteria);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNewsTag(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteNewsTags(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNewsAuthor(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteNewsAuthor(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public void setNewsDAO(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }
}
