package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;

import java.util.List;

public interface NewsService {

    /**
     * Adds news to the data source and returns the generated id.
     *
     * @param news
     *        News to add
     *
     * @return generated id
     *
     * @throws ServiceException in case of ServiceException
     *
     */
    long addNews(News news) throws ServiceException;

    /**
     * Edits the information about news in the data source.
     *
     * @param news
     *        News to edit
     *
     * @throws ServiceException in case of ServiceException
     *
     */
    void editNews(News news) throws ServiceException;

    /**
     * Deletes news with the given id from the data source.
     *
     * @param newsId
     *        Id of the news to delete
     *
     * @throws ServiceException in case of ServiceException
     *
     */
    void deleteNews(long newsId) throws ServiceException;

    /**
     * Deletes news with the given id from the data source.
     *
     * @param newsId
     *        The list of news id to delete
     *
     * @throws ServiceException in case of ServiceException
     *
     */
    void deleteNewsList(List<Long> newsId) throws ServiceException;

    /**
     * Returns the news by the given id.
     *
     * @param newsId
     *        Id of the news
     *
     * @return the author by the id of his news
     *
     */
    News findById(long newsId) throws ServiceException;

    /**
     * Creates a link between the news and its author.
     *
     * @param  newsId
     *         The news id
     *
     * @param  authorId
     *         The id of news author
     *
     * @throws  ServiceException
     *          In case of {@code ServiceException}
     */
    void addNewsAuthor(long newsId, long authorId) throws ServiceException;

    /**
     * Creates a link between the news and its tag.
     *
     * @param  newsId
     *         The news id
     *
     * @param  tagId
     *         The id of news tag
     *
     * @throws  ServiceException
     *          In case of {@code ServiceException}
     */
    void addNewsTag(long newsId, long tagId) throws ServiceException;

    /**
     * Creates a link between the news and its tags.
     *
     * @param  newsId
     *         The news id
     *
     * @param  tagIdList
     *         The list of news tag id's
     *
     * @throws  ServiceException
     *          In case of {@code ServiceException}
     */
    void addNewsTags(long newsId, List<Long> tagIdList) throws ServiceException;

    /**
     * Returns the count of news.
     *
     * @param searchCriteria
     *         the criteria for news search
     *
     * @return  the count of news
     *
     * @throws  ServiceException
     *          In case of {@code ServiceException}
     */
    List<News> findNews(SearchCriteria searchCriteria) throws ServiceException;

     /**
      * Returns the count of news.
      *
      * @param searchCriteria
      *         the criteria for news search
      *
      * @return  the count of news
      *
      * @throws  ServiceException
      *          In case of {@code ServiceException}
      */
    Long getNewsCount(SearchCriteria searchCriteria) throws ServiceException;

    /**
     * Returns the news id list of two news in the filtered by search criteria list
     * previous and next by one to the news with the given id.
     *
     * @param searchCriteria
     *          the criteria that are used to find news
     *
     * @return  the List of news id,
     *          the first element is 0 if the news with the given id is the first in the whole filtered news list
     *          the second element is 0 if the news with the given id is the last in the whole filtered news list
     *
     * @throws  ServiceException
     *          In case of {@code DAOException}
     */
    List<Long> findPreviousAndNextNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException;

    /**
     * Deletes reference on news with the specified id
     * from the news-tag join table.
     *
     * @param  newsId
     *         The id of news which references to delete
     *
     * @throws  ServiceException
     *          In case of {@code DAOException}
     */
    void deleteNewsTag(Long newsId) throws ServiceException;

    /**
     * Deletes reference on news with the specified id
     * from the news-author join table.
     *
     * @param  newsId
     *         The id of news which references to delete
     *
     * @throws  ServiceException
     *          In case of {@code DAOException}
     */
    void deleteNewsAuthor(Long newsId) throws ServiceException;


}
