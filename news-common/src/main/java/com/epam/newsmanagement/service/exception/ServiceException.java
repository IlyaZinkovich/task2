package com.epam.newsmanagement.service.exception;

import com.epam.newsmanagement.exception.NewsManagementException;

public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = -2647527156433951368L;

    public ServiceException() {
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
