package com.epam.newsmanagement.domain;

import java.util.List;

public class SearchCriteria {
    private Long authorId;
    private List<Long> tagsId;
    private Long page;
    private Long pageSize;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public int calculateParameterNumber() {
        int parameterNumber = 0;
        parameterNumber += authorId == null ? 0 : 1;
        parameterNumber += tagsId == null ? 0 : tagsId.size();
        parameterNumber += page == null ? 0 : 2;
        parameterNumber += pageSize == null ? 0 : 2;
        return parameterNumber;
    }

}
