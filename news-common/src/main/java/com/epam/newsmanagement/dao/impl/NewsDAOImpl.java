package com.epam.newsmanagement.dao.impl;


import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.newsmanagement.dao.impl.DAOConstants.NEWS_ID;

public class NewsDAOImpl implements NewsDAO {

    private static Logger logger = Logger.getLogger(NewsDAOImpl.class);

    private DataSource dataSource;

    private final static String INSERT_NEWS_QUERY = "INSERT INTO NEWS " +
            "(NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE)" +
            " VALUES (NEWS_AI.NEXTVAL, ?, ?, ?, ?, ?)";
    private final static String INSERT_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR " +
            "(NEWS_ID, AUTHOR_ID)" +
            " VALUES (?, ?)";
    private final static String INSERT_NEWS_TAG_QUERY = "INSERT INTO NEWS_TAG " +
            "(NEWS_ID, TAG_ID)" +
            " VALUES (?, ?)";
    private final static String UPDATE_NEWS_QUERY = "UPDATE NEWS " +
        "SET SHORT_TEXT = ?, FULL_TEXT = ?, TITLE = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? " +
        "WHERE NEWS_ID = ?";
    private final static String DELETE_NEWS_TAG_QUERY = "DELETE NEWS_TAG WHERE NEWS_ID = ?";
    private final static String DELETE_NEWS_AUTHOR_QUERY = "DELETE NEWS_AUTHOR WHERE NEWS_ID = ?";
    private final static String DELETE_NEWS_COMMENTS_QUERY = "DELETE COMMENTS WHERE NEWS_ID = ?";
    private final static String DELETE_NEWS_QUERY = "DELETE NEWS WHERE NEWS_ID = ?";

    private final static String FIND_NEWS_FOR_PAGE_OF_SIZE_BEGIN = " SELECT * FROM " +
            " (SELECT ALL_NEWS.*, ROWNUM ROWNUM__ " +
            "  FROM " +
            "  ( ";
    private final static String FIND_NEWS_FOR_PAGE_OF_SIZE_END = "  ) ALL_NEWS " +
            "  WHERE ROWNUM < ((? * ?) + 1 ) " +
            " ) " +
            " WHERE ROWNUM__ >= (((? - 1) * ?) + 1)";
    private final static String FIND_NEWS_BEGIN = " SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, " +
            " NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, COUNT(COMMENTS.comment_id) AS NUMBER_OF_COMMENTS FROM NEWS " +
            " LEFT JOIN COMMENTS " +
            " ON COMMENTS.NEWS_ID=NEWS.NEWS_ID ";
    private final static String FIND_NEWS_END = " GROUP BY NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, " +
            " NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ORDER BY NUMBER_OF_COMMENTS DESC, NEWS.MODIFICATION_DATE ASC";
    private final static String WHERE = " WHERE ";
    private final static String AND = " AND ";
    private final static String NEWS_WITH_AUTHOR_EXISTS = " EXISTS(SELECT NEWS_AUTHOR.NEWS_ID FROM NEWS_AUTHOR " +
            " WHERE NEWS_AUTHOR.AUTHOR_ID=? AND NEWS_AUTHOR.NEWS_ID=NEWS.NEWS_ID) ";
    private final static String NEWS_WITH_TAGS_EXISTS_BEGIN = " EXISTS(SELECT NEWS_TAG.NEWS_ID FROM NEWS_TAG " +
            "WHERE NEWS_TAG.TAG_ID IN (";
    private final static String NEWS_WITH_TAGS_EXISTS_END = ") AND NEWS_TAG.NEWS_ID=NEWS.NEWS_ID) ";

    private final static String GET_NEWS_COUNT_BEGIN = "SELECT COUNT(NEWS_ID) AS NEWS_COUNT FROM(";
    private final static String GET_NEWS_COUNT_END = ")";

    private final static String GET_PREVIOUS_AND_NEXT_NEWS_ID_BEGIN = " SELECT PREVIOUS_NEWS_ID, NEXT_NEWS_ID FROM ( " +
            " SELECT NEWS_ID, ROWNUM__, LAG(NEWS_ID, 1, 0) OVER (ORDER BY ROWNUM__) AS PREVIOUS_NEWS_ID, " +
            " LEAD(NEWS_ID, 1, 0) OVER (ORDER BY ROWNUM__) AS NEXT_NEWS_ID FROM " +
            " (SELECT ALL_NEWS.*, ROWNUM ROWNUM__ " +
            " FROM " +
            " ( ";
    private final static String GET_PREVIOUS_AND_NEXT_NEWS_ID_END = " ) " +
            "ALL_NEWS " +
            ")) WHERE NEWS_ID=?";

    private final static String SELECT_NEWS_BY_ID_QUERY = "SELECT * FROM NEWS WHERE NEWS_ID = ?";

    private PreparedStatement prepareStatementForUpdate(Connection connection, News news) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_NEWS_QUERY);
        preparedStatement.setString(1, news.getShortText());
        preparedStatement.setString(2, news.getFullText());
        preparedStatement.setString(3, news.getTitle());
        preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
        preparedStatement.setDate(5, new Date(news.getModificationDate().getTime()));
        preparedStatement.setLong(6, news.getId());
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForInsert(Connection connection, News news) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEWS_QUERY, new String[]{NEWS_ID});
        preparedStatement.setString(1, news.getShortText());
        preparedStatement.setString(2, news.getFullText());
        preparedStatement.setString(3, news.getTitle());
        preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
        if (news.getModificationDate() != null) {
            preparedStatement.setDate(5, new Date(news.getModificationDate().getTime()));
        } else {
            preparedStatement.setDate(5, null);
        }
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForDelete(Connection connection, Long newsId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_QUERY);
        preparedStatement.setLong(1, newsId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForDeleteNewsAuthor(Connection connection, Long newsId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_AUTHOR_QUERY);
        preparedStatement.setLong(1, newsId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindNews(Connection connection, SearchCriteria searchCriteria) throws SQLException {
        String findNewsQuery = buildFindNewsQuery(searchCriteria);
        PreparedStatement preparedStatement = connection.prepareStatement(findNewsQuery);
        setSearchParameters(searchCriteria, preparedStatement);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindPreviousAndNextNews(Connection connection, Long newsId, SearchCriteria searchCriteria) throws SQLException {
        searchCriteria.setPage(null);
        searchCriteria.setPageSize(null);
        StringBuilder findPreviousAndNextNewsQueryBuilder = new StringBuilder();
        findPreviousAndNextNewsQueryBuilder.append(GET_PREVIOUS_AND_NEXT_NEWS_ID_BEGIN);
        String findNewsQuery = buildFindNewsQuery(searchCriteria);
        findPreviousAndNextNewsQueryBuilder.append(findNewsQuery).append(GET_PREVIOUS_AND_NEXT_NEWS_ID_END);
        PreparedStatement preparedStatement = connection.prepareStatement(findPreviousAndNextNewsQueryBuilder.toString());
        setSearchParameters(searchCriteria, preparedStatement);
        int lastParameterIndex = 1 + searchCriteria.calculateParameterNumber();
        preparedStatement.setLong(lastParameterIndex, newsId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForGetNewsCount(Connection connection, SearchCriteria searchCriteria) throws SQLException {
        StringBuilder getNewsCountQueryBuilder = new StringBuilder();
        searchCriteria.setPage(null);
        searchCriteria.setPageSize(null);
        String findNewsQuery = buildFindNewsQuery(searchCriteria);
        getNewsCountQueryBuilder.append(GET_NEWS_COUNT_BEGIN).append(findNewsQuery).append(GET_NEWS_COUNT_END);
        PreparedStatement preparedStatement = connection.prepareStatement(getNewsCountQueryBuilder.toString());
        setSearchParameters(searchCriteria, preparedStatement);
        return preparedStatement;
    }

    private void setSearchParameters(SearchCriteria searchCriteria, PreparedStatement preparedStatement) throws SQLException {
        int parameterIndex = 1;
        if (searchCriteria.getAuthorId() != null) {
            preparedStatement.setLong(parameterIndex++, searchCriteria.getAuthorId());
        }
        if (searchCriteria.getTagsId() != null) {
            List<Long> tagsId = searchCriteria.getTagsId();
            for (Long tagId : tagsId) {
                preparedStatement.setLong(parameterIndex++, tagId);
            }
        }
        if (searchCriteria.getPage() != null && searchCriteria.getPageSize() != null) {
            preparedStatement.setLong(parameterIndex++, searchCriteria.getPage());
            preparedStatement.setLong(parameterIndex++, searchCriteria.getPageSize());
            preparedStatement.setLong(parameterIndex++, searchCriteria.getPage());
            preparedStatement.setLong(parameterIndex, searchCriteria.getPageSize());
        }
    }

    private String buildFindNewsQuery(SearchCriteria searchCriteria) {
        boolean isFirstWhereStatement = true;
        StringBuilder queryBuilder = new StringBuilder();
        if (searchCriteria.getPage() != null && searchCriteria.getPageSize() != null) {
            queryBuilder.append(FIND_NEWS_FOR_PAGE_OF_SIZE_BEGIN);
        }
        queryBuilder.append(FIND_NEWS_BEGIN);
        if (searchCriteria.getAuthorId() != null) {
            isFirstWhereStatement = appendAndStatement(isFirstWhereStatement, queryBuilder);
            queryBuilder.append(NEWS_WITH_AUTHOR_EXISTS);
        }
        if (searchCriteria.getTagsId() != null) {
            isFirstWhereStatement = appendAndStatement(isFirstWhereStatement, queryBuilder);
            queryBuilder.append(NEWS_WITH_TAGS_EXISTS_BEGIN);
            String questionMarks = searchCriteria.getTagsId().stream().map(t -> "?").collect(Collectors.joining(", "));
            queryBuilder.append(questionMarks).append(NEWS_WITH_TAGS_EXISTS_END);
        }
        queryBuilder.append(FIND_NEWS_END);
        if (searchCriteria.getPage() != null && searchCriteria.getPageSize() != null) {
            queryBuilder.append(FIND_NEWS_FOR_PAGE_OF_SIZE_END);
        }
        return queryBuilder.toString();
    }

    private boolean appendAndStatement(boolean isFirstWhereStatement, StringBuilder queryBuilder) {
        queryBuilder.append(isFirstWhereStatement ? WHERE : AND );
        return false;
    }

    private PreparedStatement prepareStatementForDeleteNewsTag(Connection connection, Long newsId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_TAG_QUERY);
        preparedStatement.setLong(1, newsId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForDeleteNewsComments(Connection connection, Long newsId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_COMMENTS_QUERY);
        preparedStatement.setLong(1, newsId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindById(Connection connection, Long id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_NEWS_BY_ID_QUERY);
        preparedStatement.setLong(1, id);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForInsertIntoNewsTag(Connection connection, Long newsId, Long tagId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEWS_TAG_QUERY);
        preparedStatement.setLong(1, newsId);
        preparedStatement.setLong(2, tagId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForInsertIntoNewsAuthor(Connection connection, Long newsId, Long authorId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEWS_AUTHOR);
        preparedStatement.setLong(1, newsId);
        preparedStatement.setLong(2, authorId);
        return preparedStatement;
    }

    private List<News> parseResultSetToList(ResultSet resultSet) throws SQLException {
        List<News> list = new LinkedList<>();
        while (resultSet.next()) {
            News news = new News();
            news.setId(resultSet.getLong("NEWS_ID"));
            news.setShortText(resultSet.getString("SHORT_TEXT"));
            news.setFullText(resultSet.getString("FULL_TEXT"));
            news.setTitle(resultSet.getString("TITLE"));
            news.setCreationDate(new java.util.Date(resultSet.getTimestamp("CREATION_DATE").getTime()));
            news.setModificationDate(new java.util.Date(resultSet.getDate("MODIFICATION_DATE").getTime()));
            list.add(news);
        }
        return list;
    }

    private News parseResultSetToObject(ResultSet resultSet) throws SQLException {
        News news = null;
        if (resultSet.next()) {
            news = new News();
            news.setId(resultSet.getLong("NEWS_ID"));
            news.setShortText(resultSet.getString("SHORT_TEXT"));
            news.setFullText(resultSet.getString("FULL_TEXT"));
            news.setTitle(resultSet.getString("TITLE"));
            news.setCreationDate(new java.util.Date(resultSet.getTimestamp("CREATION_DATE").getTime()));
            news.setModificationDate(new java.util.Date(resultSet.getDate("MODIFICATION_DATE").getTime()));
        }
        return news;
    }

    @Override
    public void insertNewsAuthor(Long newsId, Long authorId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForInsertIntoNewsAuthor(connection, newsId, authorId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void insertNewsTag(Long newsId, Long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForInsertIntoNewsTag(connection, newsId, tagId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    @Override
    public void insertNewsTags(Long newsId, List<Long> tagIdList) throws DAOException {
        for (Long tagId : tagIdList) insertNewsTag(newsId, tagId);
    }

    @Override
    public News findById(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try(PreparedStatement preparedStatement = prepareStatementForFindById(connection, newsId);
            ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToObject(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Long insert(News news) throws DAOException {
        Long lastInsertId = 0L;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForInsert(connection, news)) {
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet != null && resultSet.next())
                    lastInsertId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return lastInsertId;
    }

    @Override
    public void update(News news) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForUpdate(connection, news)) {
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForDelete(connection, newsId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsTags(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForDeleteNewsTag(connection, newsId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsComments(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForDeleteNewsComments(connection, newsId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsAuthor(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForDeleteNewsAuthor(connection, newsId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findNews(SearchCriteria searchCriteria) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindNews(connection, searchCriteria);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToList(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Long> findPreviousAndNextNews(Long newsId, SearchCriteria searchCriteria) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        List<Long> previousAndNextIdList = new LinkedList<>();
        try (PreparedStatement preparedStatement = prepareStatementForFindPreviousAndNextNews(connection, newsId,
                searchCriteria);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                previousAndNextIdList.add(resultSet.getLong("PREVIOUS_NEWS_ID"));
                previousAndNextIdList.add(resultSet.getLong("NEXT_NEWS_ID"));
                break;
            }
            return previousAndNextIdList;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Long getNewsCount(SearchCriteria searchCriteria) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForGetNewsCount(connection, searchCriteria);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            if(resultSet.next()){
                return resultSet.getLong("NEWS_COUNT");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return 0L;
    }


    @Override
    public List<News> findAll() throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}