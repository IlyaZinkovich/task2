package com.epam.newsmanagement.dao.exception;

import com.epam.newsmanagement.exception.NewsManagementException;

public class DAOException extends NewsManagementException {
    private static final long serialVersionUID = 398015075332272981L;

    public DAOException() {
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
