package com.epam.newsmanagement.dao.impl;

class DAOConstants {
    public static final int NULL_ID = 0;
    public static final String USER_ID = "USER_ID";
    public static final String NEWS_ID = "NEWS_ID";
    public static final String TAG_ID = "TAG_ID";
    public static final String AUTHOR_ID = "AUTHOR_ID";
    public static final String COMMENT_ID = "COMMENT_ID";
}
