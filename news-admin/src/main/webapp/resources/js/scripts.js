$(document).ready(function(){
    $('[id^=Edit]').click(function(){
        var id = $(this).attr('id');
        id = id.substring(4,id.length);
        $("#Edit" + id).hide();
        $("#Update" + id).show();
        $("#Expire" + id).show();
        $("#Cancel" + id).show();
        $("#Textfield" + id).prop('disabled', false);
    });
    $('[id^=Cancel]').click(function(){
        var id = $(this).attr('id');
        id = id.substring(6,id.length);
        $("#Edit" + id).show();
        $("#Update" + id).hide();
        $("#Expire" + id).hide();
        $("#Cancel" + id).hide();
        $("#Textfield" + id).prop('disabled', true);
    });
    $('[id^=Update]').click(function(){
        var id = $(this).attr('id');
        id = id.substring(6,id.length);
        var name = $("#Textfield" + id).val();
        $.post( "/news-admin/add-update-authors/update", { authorId: id, authorName: name, expired: false } );
    });
    $('[id^=Expire]').click(function(){
        var curId = $(this).attr('id');
        id = curId.substring(6,curId.length);
        var name = $("#Textfield" + id).val();
        $.post( "/news-admin/add-update-authors/update", { authorId: id, authorName: name, expired: true } );
    });
});