<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>

<!-- Include the basic JQuery support (core and ui) -->
<script type="text/javascript" src="<c:url value='/resources/js/jquery-1.6.1.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery-ui-1.8.13.custom.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/ui.dropdownchecklist-1.4-min.js'/>"></script>


<script type="text/javascript" src="<c:url value='/resources/bower_components/jquery/dist/jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/bower_components/moment/min/moment.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/bower_components/bootstrap/dist/js/bootstrap.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'/>"></script>
<link rel="stylesheet" href="<c:url value='/resources/bower_components/bootstrap/dist/css/bootstrap.min.css'/>" />
<link rel="stylesheet" href="<c:url value='/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css'/>" />

<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui.dropdownchecklist.standalone.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/dropdowns.css'/>">
<script type="text/javascript" src="<c:url value='/resources/js/dropdowns.js'/>"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>


<c:choose>
    <c:when test="${pageContext.response.locale eq 'ru'}">
        <fmt:formatDate value="${news.creationDate}" pattern="YYYY-MM-dd" var="formattedCreationDate" />
        <fmt:formatDate value="${news.modificationDate}" pattern="YYYY-MM-dd" var="formattedModificationDate" />
    </c:when>
    <c:otherwise>
        <fmt:formatDate value="${news.creationDate}" pattern="YYYY-MM-dd" var="formattedCreationDate" />
        <fmt:formatDate value="${news.modificationDate}" pattern="YYYY-MM-dd" var="formattedModificationDate" />
    </c:otherwise>
</c:choose>
<form:form method="POST" commandName="news" action="/news-admin/add-update-news" >
    <div class="input-group">
        <form:hidden path="newsId" id="newsId" value="${news.newsId}" />
        <div class="form-group">
            <label for="title"><spring:message code='add-update-news.title' /></label>
            <form:input path="title" id="title" cssClass="form-control" />
        </div>
        <div class="form-group">
            <label for="creationDate"><spring:message code='add-update-news.creationDate' /></label>
            <fmt:formatDate value="${news.creationDate}" pattern="MM/dd/YYYY" var="formattedCreationDate" />
            <form:input id="creationDate" path="creationDate" value="${formattedCreationDate}" cssClass="form-control" />

        </div>
        <c:if test="${news.newsId != null}" >
        <div class="form-group">
            <label for="modificationDate"><spring:message code='add-update-news.modificationDate' /></label>
            <fmt:formatDate value="${news.modificationDate}" pattern="MM/dd/YYYY" var="formattedModificationDate" />
            <form:input id="modificationDate" path="modificationDate" value="${formattedModificationDate}" cssClass="form-control" />
        </div>
        </c:if>
        <div class="form-group">
            <label for="brief"><spring:message code='add-update-news.brief' /></label>
            <form:textarea path="brief" id="brief" rows="5" cssClass="form-control" />
        </div>
        <div class="form-group">
            <label for="content"><spring:message code='add-update-news.content' /></label>
            <form:textarea path="content" id="content" rows="5" cssClass="form-control" />
        </div>
        <br>

        <div class="form-group">
            <label for="authorId"><spring:message code='add-update-news.author' /></label>
            <form:select path="authorId" cssClass="form-control" >
                <form:option value="" ><spring:message code='filter.author.placeholder' /></form:option>
                <form:options items="${authorList}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
        <br>
        <div class="form-group">
            <label for="tagsId"><spring:message code='add-update-news.tags' /></label>
            <form:select multiple="multiple" path="tagsId">
                <form:options items="${tagList}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
    </div>



    <span class="input-group-btn">
        <button class="btn btn-default" type="submit" value="Save"><spring:message code='add-update-news.save' /></span></button>
    </span>

</form:form>

<script type="text/javascript">
        $(document).ready(function() {
            $("#tagsId").multiselect({
                buttonText: function(options, select) {
                    if (options.length === 0) {
                        return "<spring:message code='filter.tags.placeholder' />";
                    }
                    else if (options.length > 3) {
                        return "<spring:message code='filter.tags.placeholder.more' />";
                    }
                    else {
                        var labels = [];
                        options.each(function() {
                            if ($(this).attr('label') !== undefined) {
                                labels.push($(this).attr('label'));
                            }
                            else {
                                labels.push($(this).html());
                            }
                        });
                        return labels.join(', ') + '';
                    }
                }
            });
            $('#modificationDate').datetimepicker({
                format: "<spring:message code='date.format' />"
            });
            $('#creationDate').datetimepicker({
                format: "<spring:message code='date.format' />"
            });
            $("select+div.btn-group").click(function(){
                if ($("select+div.btn-group").attr('class') === "btn-group") {
                    $("select+div.btn-group").attr('class', 'btn-group open');
                } else if ($("select+div.btn-group").attr('class') === "btn-group open") {
                    $("select+div.btn-group").attr('class', 'btn-group');
                }
            });

            $.validator.addMethod("valueNotEquals", function(value, element, arg){
                    return arg != value;
                }, "<spring:message code='add-update-news.please-select-author' />");

                $("#news").validate(
                        {
                          rules: {
                            title: {
                              required : true,
                              minlength : 3,
                              maxlength : 30
                            },
                            date: {
                              required : true,
                              dateFormat : true
                            },
                            brief: {
                              required : true,
                              minlength : 3,
                              maxlength : 100
                            },
                            content: {
                              required : true,
                              minlength : 3,
                              maxlength : 2000
                            },
                            authorId: {
                              valueNotEquals: ""
                            },
                            tagsId: {
                              valueNotEquals: null
                            }
                          },
                          highlight: function(element) {
                            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                          },
                          unhighlight: function(element) {
                            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                          },
                          messages: {
                              title: "<spring:message code='add-update-news.required-field' />",
                              date: "<spring:message code='add-update-news.required-field' />",
                              brief: "<spring:message code='add-update-news.required-field' />",
                              content: "<spring:message code='add-update-news.required-field' />"
                          }
                        }
                );
        });
</script>
