<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>

<style>
html {
  position: relative;
  min-height: 100%;
}
body {
  /* Margin bottom by footer height */
  margin-bottom: 60px;
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 30px;
  background-color: #f5f5f5;
}
</style>
<nav class="navbar navbar-default navbar-fixed-bottom">
<div class="row navbar-inner">
    <p align="center">
        <spring:message code='footer.copyright' />
    </p>
</div>
</nav>