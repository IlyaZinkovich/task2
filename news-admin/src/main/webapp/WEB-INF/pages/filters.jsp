<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>

<!-- Include the basic JQuery support (core and ui) -->
<script type="text/javascript" src="<c:url value='/resources/js/jquery-1.6.1.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery-ui-1.8.13.custom.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/ui.dropdownchecklist-1.4-min.js'/>"></script>


<script type="text/javascript" src="<c:url value='/resources/bower_components/jquery/dist/jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/bower_components/moment/min/moment.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/bower_components/bootstrap/dist/js/bootstrap.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'/>"></script>
<link rel="stylesheet" href="<c:url value='/resources/bower_components/bootstrap/dist/css/bootstrap.min.css'/>" />
<link rel="stylesheet" href="<c:url value='/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css'/>" />

<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui.dropdownchecklist.standalone.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/dropdowns.css'/>">
<script type="text/javascript" src="<c:url value='/resources/js/dropdowns.js'/>"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script type="text/javascript">
        $(document).ready(function() {
            $("#tagsId").multiselect({
                buttonText: function(options, select) {
                    if (options.length === 0) {
                        return "<spring:message code='filter.tags.placeholder' />";
                    }
                    else if (options.length > 3) {
                        return "<spring:message code='filter.tags.placeholder.more' />";
                    }
                    else {
                        var labels = [];
                        options.each(function() {
                            if ($(this).attr('label') !== undefined) {
                                labels.push($(this).attr('label'));
                            }
                            else {
                                labels.push($(this).html());
                            }
                        });
                        return labels.join(', ') + '';
                    }
                }
            });
            $("select+div.btn-group").click(function(){
                if ($("select+div.btn-group").attr('class') === "btn-group") {
                    $("select+div.btn-group").attr('class', 'btn-group open');
                } else if ($("select+div.btn-group").attr('class') === "btn-group open") {
                    $("select+div.btn-group").attr('class', 'btn-group');
                }
            });
        });
</script>
<div class="row">
    <div class="col-md-9 col-md-offset-3">
        <form:form method="POST" modelAttribute="filter" cssClass="navbar-form" action="/news-admin/news-list/filter" >
                <form:select path="authorId" cssClass="form-control">
                    <form:option value=""><spring:message code='filter.author.placeholder' /></form:option>
                    <form:options items="${authorList}" itemLabel="name" itemValue="id" />
                </form:select>

                <div class="form-group">
                    <form:select multiple="multiple" path="tagsId">
                        <form:options items="${tagList}" itemLabel="name" itemValue="id" />
                    </form:select>
                </div>
                <button type="submit" class="btn btn-default" value="Submit"><spring:message code='filter.filter' /></button>
                <a href="<spring:url value="/news-list/filter/reset"/>" class="btn btn-default"><spring:message code='filter.reset' /></a>
        </form:form>
    </div>
</div>
