<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>
<script type="text/javascript" src="<c:url value='/resources/js/jquery-1.6.1.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery-ui-1.8.13.custom.min.js'/>"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="<c:url value='/resources/js/ui.dropdownchecklist-1.4-min.js'/>"></script>

<style>
#comments, #comments li {
    margin: 0;
    padding: 0;
}
#comment-text {
    background-color:#C0C0C0;
}
#comments li {
    list-style: none;
}
</style>
<div class="row">
    <div class="col-md-12 column">
        <a href='<spring:url value="/news-list.html" />' class="btn pull-left" /><spring:message code='news-full.back' /></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12 column">
        <h2>
            <b>${news.news.title}</b>
        </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12 column">
        <p class="pull-left">(<spring:message code='news-full.by' /> ${news.author.name})</p>
        <c:choose>
            <c:when test="${pageContext.response.locale eq 'ru'}">
                <fmt:formatDate value="${news.news.creationDate}" pattern="dd/MM/YYYY" var="formattedDate" />
            </c:when>
            <c:otherwise>
                <fmt:formatDate value="${news.news.creationDate}" pattern="MM/dd/YYYY" var="formattedDate" />
            </c:otherwise>
        </c:choose>
        <p class="pull-right">${formattedDate}</p>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#commentForm").validate(
                {
                  rules: {
                    commentText: {
                      required : true
                    }
                  },
                  highlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                  },
                  unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                  },
                  messages: {
                      commentText: "<spring:message code='add-update-news.required-field' />",
                  }
                }
        );
      });
</script>
<div class="row clearfix">
    <div class="col-md-12 column">
        <p>
            ${news.news.fullText}
        </p>
    </div>
</div>
<br>
<c:forEach var="comment" items = "${news.comments}">
    <div class="row">
        <div class="col-md-4">
            <ul id="comments" class="comments">
                <li class="comment">
                    <c:choose>
                        <c:when test="${pageContext.response.locale eq 'ru'}">
                            <fmt:formatDate value="${comment.creationDate}" pattern="dd/MM/YYYY" var="commentDate" />
                        </c:when>
                        <c:otherwise>
                            <fmt:formatDate value="${comment.creationDate}" pattern="MM/dd/YYYY" var="commentDate" />
                        </c:otherwise>
                    </c:choose>
                    <p class="pull-left">${commentDate}</p>
                    <div class="clearfix" style="background-color:#C0C0C0">
                        <form:form method="POST" commandName="deleteCommentForm" action="/news-admin/view-news/${news.news.id}/delete-comment" >
                            <form:hidden path="newsId" value="${news.news.id}" />
                            <form:hidden path="id" value="${comment.id}" />
                            <button type="submit" class="btn btn-danger green btn-sm pull-right">X</button>
                        </form:form>
                        <em>${comment.commentText}</em>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <br>
</c:forEach>

<div class="form-group">
    <form:form method="POST" commandName="commentForm" action="/news-admin/view-news/${news.news.id}/add-comment" >
        <div class="row">
            <div class="col-md-8">
                <form:textarea path="commentText" cssClass="required" />
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6" />
            <div class="col-md-2">
                <form>
                    <button type="submit" class="btn btn-success green btn-sm"><spring:message code='news-full.post-comment' /></button>
                </form>
            </div>
            <div class="col-md-6" />
        </div>
    </form:form>
</div>
<nav class="navbar navbar-default navbar-fixed-bottom">
<div class="row navbar-inner">
    <div class="col-md-9 col-md-offset-2" >
        <c:if test="${previous != '0'}">
            <a href='<spring:url value="/view-news/${previous}.html" />' class="btn pull-left" /><spring:message code='news-full.previous' /></a>
        </c:if>
        <c:if test="${next != '0'}">
            <a href='<spring:url value="/view-news/${next}.html" />' class="btn pull-right" /><spring:message code='news-full.next' /></a>
        </c:if>
    </div>
</div>

<div class="row navbar-inner">
    <p align="center">
        <spring:message code='footer.copyright' />
    </p>
</div>
</nav>
