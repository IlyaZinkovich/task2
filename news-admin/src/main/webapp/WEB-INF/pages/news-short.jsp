<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>
<script>
    $(document).ready(function(){
        $('#deleteNews').click(function(){
            if($('.toDelete:checked').length == 0) {
                alert("<spring:message code='news-list.delete-news-check' />");
            } else {
                $( "#deleteForm" ).submit();
            }
        });
    });
</script>
<form method="POST" id="deleteForm" action="/news-admin/news-list/delete" >
<c:forEach items="${newsList}" var="newsItem">
<div>
    <div class="row">
        <div class="col-md-8 column">
            <strong>
                <a href='<spring:url value="/view-news/${newsItem.news.id}.html" />' />${newsItem.title}</a>
            </strong>
        </div>
        <div class="col-md-2 column">
            <p>(<spring:message code='news-short.by' /> ${newsItem.author.name})</p>
        </div>
        <div class="col-md-2 column">
            <c:choose>
                <c:when test="${pageContext.response.locale eq 'ru'}">
                    <fmt:formatDate value="${newsItem.creationDate}" pattern="dd/MM/YYYY" var="formattedDate" />
                </c:when>
                <c:otherwise>
                    <fmt:formatDate value="${newsItem.creationDate}" pattern="MM/dd/YYYY" var="formattedDate" />
                </c:otherwise>
            </c:choose>
            <u class="pull-right">${formattedDate}</u>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12 column">
            <p>${newsItem.shortText}</p>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-7 column">
            <p class="text-muted pull-right">
                <c:forEach items="${newsItem.tags}" var="tagItem">
                    ${tagItem.name}
                </c:forEach>
            </p>
        </div>
        <div class="col-md-2 column">
            <p class="text-warning pull-right">
                <spring:message code='news-short.comments' />(${newsItem.comments.size()})
            </p>
        </div>
        <div class="col-md-2 column">
            <a class="pull-right" href='<spring:url value="/add-update-news/${newsItem.news.id}.html" />' /><spring:message code='news-short.edit' /></a>
        </div>
        <div class="col-md-1 column">
                <input type="checkbox" class="toDelete" name="newsIdToDelete" value="${newsItem.news.id}"/>
        </div>
    </div>
</div>
</c:forEach>
<br>
<c:choose>
  <c:when test="${!newsList.isEmpty()}">
    <div class="row">
        <div class="col-md-2 pull-right">
            <a id="deleteNews" class="btn btn-danger"><spring:message code='news-short.delete' /></a>
        </div>
    </div>
  </c:when>
  <c:otherwise>
    <h1><spring:message code='news.news-not-found' /><h1>
  </c:otherwise>
</c:choose>
</form>