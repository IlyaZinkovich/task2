<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>

<!-- Include the basic JQuery support (core and ui) -->
<script type="text/javascript" src="<c:url value='/resources/js/jquery-1.6.1.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery-ui-1.8.13.custom.min.js'/>"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="<c:url value='/resources/js/ui.dropdownchecklist-1.4-min.js'/>"></script>

<script>
    $(document).ready(function(){
        $('[id^=Edit]').click(function(){
            var id = $(this).attr('id');
            id = id.substring(4,id.length);
            $("#Edit" + id).hide();
            $("#Update" + id).show();
            $("#Expire" + id).show();
            $("#Cancel" + id).show();
            $("#Textfield" + id).prop('disabled', false);
        });

        $('[id^=Cancel]').click(function(){
            var id = $(this).attr('id');
            id = id.substring(6,id.length);
            $("#Edit" + id).show();
            $("#Update" + id).hide();
            $("#Expire" + id).hide();
            $("#Cancel" + id).hide();
            $("#Textfield" + id).prop('disabled', true);
        });
        $('[id^=Update]').click(function(){
            var id = $(this).attr('id');
            id = id.substring(6,id.length);
            var name = $("#Textfield" + id).val();
            $.post( "/news-admin/add-update-authors/update", { authorId: id, authorName: name, expired : null} );
            location.reload();
        });
        $('[id^=Expire]').click(function(){
            var curId = $(this).attr('id');
            id = curId.substring(6,curId.length);
            var name = $("#Textfield" + id).val();
            $.post( "/news-admin/add-update-authors/update", { authorId: id, authorName: name, expired: new Date().getTime() } );
            location.reload();
        });
        $("#addAuthorForm").validate(
            {
              rules: {
                name: {
                  required : true,
                  maxlength : 30
                }
              },
              highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
              },
              unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
              },
              messages: {
                  name: "<spring:message code='add-update-author.required-field' />"
              }
            }
        );
    });
</script>


<form:form method="POST" commandName="updateAuthorForm" action="/news-admin/add-update-authors/update" >
    <c:forEach var="author" items = "${authorListNotExpired}">
        <div class="row">
        <div class="col-md-12">
        <label><spring:message code='add-update-author.author' /></label>
        <input type="text" id="Textfield${author.id}" rows="1" disabled="disabled" value="${author.name}" maxlength="30" required>
        <a class="btn" id="Edit${author.id}"><spring:message code='add-update-author.edit' /></a>
        <a class="btn" id="Update${author.id}" style="display: none;" ><spring:message code='add-update-author.update' /></button>
        <a class="btn" id="Expire${author.id}" style="display: none;" ><spring:message code='add-update-author.expire' /></button>
        <a class="btn" id="Cancel${author.id}" style="display: none;" ><spring:message code='add-update-author.cancel' /></a>
        </div>
        </div>
    </c:forEach>
</form:form>

<div class="row">
<div class="col-md-12">
<form:form method="POST" commandName="addAuthorForm" action="/news-admin/add-update-authors/add" >
    <label><spring:message code='add-update-author.add-author' /></label>
    <div class="form-group">
        <input type="hidden" name="id" value="null">
        <input type="text" id="name" name="name" size="30">
    </div>
    <input type="submit" name="Save" value="<spring:message code='add-update-author.save' />">
</form:form>
</div>
</div>

