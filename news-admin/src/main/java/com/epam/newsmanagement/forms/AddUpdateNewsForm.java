package com.epam.newsmanagement.forms;



import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class AddUpdateNewsForm implements Serializable {

    private static final long serialVersionUID = -5471355180206373023L;
    private Long newsId;

    @NotNull
    @Size(max = 30, message = "title")
    private String title;

    private Date creationDate;

    @NotNull
    @Size(max = 100)
    private String brief;

    @NotNull
    @Size(max = 2000)
    private String content;

    private Date modificationDate;

    @NotNull
    private Long authorId;

    @NotNull
    private List<Long> tagsId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    @Override
    public String toString() {
        return "AddNewsForm{" +
                "title='" + title + '\'' +
                ", creationDate=" + creationDate +
                ", brief='" + brief + '\'' +
                ", content='" + content + '\'' +
                ", authorId=" + authorId +
                ", tagsId=" + tagsId +
                '}';
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public Date getModificationDate() {
        return new Date();
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }
}
