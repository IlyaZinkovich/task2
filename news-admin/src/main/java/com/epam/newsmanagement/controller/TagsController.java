package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.forms.Filter;
import com.epam.newsmanagement.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class TagsController {

    @Autowired
    private TagService tagService;

    @RequestMapping( value = "/add-update-tags", method = RequestMethod.GET )
    public String addUpdateTags(Model model){
        List<Tag> tagList = tagService.findAll();
        model.addAttribute("tagList", tagList);
        model.addAttribute("current", "add-update-tags");
        return "add-update-tags";
    }

    @RequestMapping( value = "/add-update-tags/update", method = RequestMethod.POST )
    public String updateTag(@ModelAttribute Tag tag, BindingResult result){
        tagService.editTag(tag);
        return "redirect:/add-update-tags";
    }

    @RequestMapping( value = "/add-update-tags/add", method = RequestMethod.POST )
    public String addTag(@ModelAttribute Tag tag, BindingResult result){
        tagService.addTag(tag);
        return "redirect:/add-update-tags";
    }

    @RequestMapping( value = "/add-update-tags/delete", method = RequestMethod.POST )
    public String deleteTag(Model model, @RequestParam("id") Long id, @ModelAttribute Filter filter){
        tagService.deleteTag(id);
        if (filter.getTagsId() != null) {
            filter.getTagsId().remove(id);
            model.addAttribute("filter", filter);
        }
        return "redirect:/add-update-tags";
    }
}
