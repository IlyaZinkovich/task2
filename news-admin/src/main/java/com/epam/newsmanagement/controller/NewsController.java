package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.*;
import com.epam.newsmanagement.forms.*;
import com.epam.newsmanagement.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@SessionAttributes({"filter"})
public class NewsController {

    public final static Long FIRST_PAGE = 1L;
    public final static Long DEFAULT_PAGE_SIZE = 4L;
    private static Logger logger = Logger.getLogger(NewsController.class);

    @Autowired
    private NewsService newsService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private NewsManagementService newsManagementService;

    @ModelAttribute("filter")
    public Filter initFilterForm() {
        return new Filter();
    }

    @ModelAttribute("commentForm")
    public Comment initCommentForm() {
        return new Comment();
    }

    @ModelAttribute("addUpdateNewsForm")
    public AddUpdateNewsForm initAddUpdateNewsForm() {
        return new AddUpdateNewsForm();
    }

    @ModelAttribute("newsIdList")
    public List<Long> initNewsIdToDeleteForm() {
        return new LinkedList<>();
    }

    @ModelAttribute("deleteCommentForm")
    public Comment initDeleteCommentForm() {
        return new Comment();
    }


//    @RequestMapping(value = "/add-update-news", method = RequestMethod.POST)
//    public String addNews(@RequestParam(value = "newsId", required = false) Long newsId,
//                          @RequestParam(value = "title", required = true) String title,
//                          @RequestParam(value = "brief", required = true) String brief,
//                          @RequestParam(value = "content", required = true) String content,
//                          @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam(value = "creationDate", required = true) Date creationDate,
//                          @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam(value = "modificationDate", required = false) Date modificationDate,
//                          @RequestParam(value = "authorId", required = true) Long authorId,
//                          @RequestParam(value = "tagsId", required = true) List<Long> tagsId) {
//        News news = new News(newsId, brief, content, title,
//                creationDate, modificationDate);
//        newsManagementService.saveNews(news, authorId, tagsId);
//        return "redirect:/view-news/" + news.getId();
//    }

    @RequestMapping(value = "/add-update-news", method = RequestMethod.POST)
    public String addNews(@ModelAttribute AddUpdateNewsForm addUpdateNewsForm, BindingResult bindingResult) {
        News news = new News(addUpdateNewsForm.getNewsId(), addUpdateNewsForm.getBrief(), addUpdateNewsForm.getContent(), addUpdateNewsForm.getTitle(),
                addUpdateNewsForm.getCreationDate(), addUpdateNewsForm.getCreationDate());
        newsManagementService.saveNews(news, addUpdateNewsForm.getAuthorId(), addUpdateNewsForm.getTagsId());
        return "redirect:/view-news/" + news.getId();
    }

    @RequestMapping(value = "/add-update-news", method = RequestMethod.GET)
    public String addUpdateNews(Model model) {
        List<Author> authorList = authorService.findNotExpired();
        List<Tag> tagList = tagService.findAll();
        AddUpdateNewsForm addUpdateNewsForm = new AddUpdateNewsForm();
        addUpdateNewsForm.setCreationDate(new Date());
        model.addAttribute("news", addUpdateNewsForm);
        model.addAttribute("tagList", tagList);
        model.addAttribute("authorList", authorList);
        model.addAttribute("current", "add-update-news");
        return "add-update-news";
    }

    @RequestMapping(value = "/add-update-news/{newsId}", method = RequestMethod.GET)
    public String updateNews(Model model, @PathVariable Long newsId) {
        NewsDTO news = newsManagementService.findNewsDTOById(newsId);
        if (news != null) {
            fillAddUpdateNewsForm(model, news);
        }
        List<Author> authorList = authorService.findNotExpired();
        authorList.add(news.getAuthor());
        fillAddUpdateNewsForm(model, news);
        List<Tag> tagList = tagService.findAll();
        model.addAttribute("tagList", tagList);
        model.addAttribute("authorList", authorList);
        model.addAttribute("current", "add-update-news");
        return "add-update-news";
    }

    private void fillAddUpdateNewsForm(Model model, NewsDTO news) {
        AddUpdateNewsForm form = new AddUpdateNewsForm();
        form.setNewsId(news.getNews().getId());
        form.setTitle(news.getNews().getTitle());
        form.setBrief(news.getNews().getShortText());
        form.setContent(news.getNews().getFullText());
        form.setCreationDate(news.getNews().getCreationDate());
        form.setAuthorId(news.getAuthor().getId());
        form.setTagsId(news.getTags().stream().map(Tag::getId).collect(Collectors.toList()));
        model.addAttribute("news", form);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model, @ModelAttribute Filter filter) {
        return newsListPaged(model, FIRST_PAGE, filter);
    }

    @RequestMapping(value = "/news-list", method = RequestMethod.GET)
    public String newsList(Model model, @ModelAttribute Filter filter) {
        return newsListPaged(model, FIRST_PAGE, filter);
    }

    @RequestMapping(value = "/news-list/{page}", method = RequestMethod.GET)
    public String newsListPaged(Model model, @PathVariable Long page, @ModelAttribute Filter filter) {
        SearchCriteria searchCriteria = filterToSearchCriteria(page, filter);
        List<NewsDTO> newsList = newsManagementService.findNewsDTO(searchCriteria);
        Long newsCount = newsService.getNewsCount(searchCriteria);
        List<Author> authorList = authorService.findHavingNews();
        List<Tag> tagList = tagService.findAll();
        Long pageCount = (long) Math.ceil((double) newsCount / DEFAULT_PAGE_SIZE);
        fillModelAttributesForNewsList(model, filter, newsList, authorList, tagList, pageCount);
        return "news-list";
    }

    private void fillModelAttributesForNewsList(Model model, @ModelAttribute Filter filter, List<NewsDTO> newsList, List<Author> authorList, List<Tag> tagList, Long pageCount) {
        model.addAttribute("newsList", newsList);
        model.addAttribute("tagList", tagList);
        model.addAttribute("authorList", authorList);
        model.addAttribute("pageCount", pageCount);
        model.addAttribute("filter", filter);
        model.addAttribute("current", "news-list");
    }

    private SearchCriteria filterToSearchCriteria(Long page, Filter filter) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthorId(filter.getAuthorId());
        searchCriteria.setTagsId(filter.getTagsId());
        searchCriteria.setPage(page);
        searchCriteria.setPageSize(DEFAULT_PAGE_SIZE);
        return searchCriteria;
    }

    @RequestMapping(value = "/news-list/delete", method = RequestMethod.POST)
    public String deleteNewsList(@RequestParam("newsIdToDelete") List<Long> newsIdToDelete) {
        newsManagementService.deleteNewsWithComments(newsIdToDelete);
        return "redirect:/news-list";
    }

    @RequestMapping(value = "/view-news/{id}", method = RequestMethod.GET)
    public String viewNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        NewsDTO newsDTO = newsManagementService.findNewsDTOById(id);
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        addPreviousAndNextNewsIdToTheModel(model, previousAndNextNews);
        model.addAttribute("news", newsDTO);
        return "view-news";
    }

    private void addPreviousAndNextNewsIdToTheModel(Model model, List<Long> previousAndNextNews) {
        if (previousAndNextNews.get(0) != null) {
            model.addAttribute("previous", previousAndNextNews.get(0));
        }
        if (previousAndNextNews.get(1) != null) {
            model.addAttribute("next", previousAndNextNews.get(1));
        }
    }


    @RequestMapping(value = "/view-news/{news-id}/delete-comment", method = RequestMethod.POST)
    public String deleteComment(@ModelAttribute(value = "deleteCommentForm") Comment comment) {
        commentService.deleteComment(comment.getId());
        return "redirect:/view-news/" + comment.getNewsId();
    }

    @RequestMapping(value = "/view-news/{newsId}/add-comment", method = RequestMethod.POST)
    public String addComment(@PathVariable Long newsId, Model model, @ModelAttribute Comment comment) {
        if (comment.getCommentText() == null) {
            return "redirect:/view-news/" + newsId;
        }
        comment.setCreationDate(new Date());
        comment.setNewsId(newsId);
        commentService.addComment(comment);
        NewsDTO newsDTO = newsManagementService.findNewsDTOById(newsId);
        model.addAttribute("news", newsDTO);
        return "redirect:/view-news/" + newsId;
    }

    @RequestMapping(value = "/view-news/{id}/previous")
    public String previousNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        NewsDTO newsDTO = newsManagementService.findNewsDTOById(previousAndNextNews.get(0));
        model.addAttribute("news", newsDTO);
        return "redirect:/view-news/" + previousAndNextNews.get(0);
    }

    @RequestMapping(value = "/view-news/{id}/next")
    public String nextNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        NewsDTO newsDTO = newsManagementService.findNewsDTOById(previousAndNextNews.get(1));
        model.addAttribute("news", newsDTO);
        return "redirect:/view-news/" + previousAndNextNews.get(1);
    }

    @RequestMapping(value = "/news-list/filter", method = RequestMethod.POST)
    public String filterNews(Model model, @ModelAttribute Filter filter, BindingResult result) {
        return newsListPaged(model, FIRST_PAGE, filter);
    }

    @RequestMapping(value = "/news-list/filter", method = RequestMethod.GET)
    public String showFilteredNews() {
        return "redirect:/news-list";
    }


    @RequestMapping(value = "/news-list/filter/reset", method = RequestMethod.GET)
    public String showNews(Model model) {
        return newsListPaged(model, FIRST_PAGE, new Filter());
    }
}
