package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.AuthorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class AuthorController {

    private static Logger logger = Logger.getLogger(AuthorController.class);

    @Autowired
    private AuthorService authorService;

    @RequestMapping( value = "/add-update-authors", method = RequestMethod.GET )
    public String addUpdateAuthors(Model model){
        List<Author> authorList = authorService.findNotExpired();
        model.addAttribute("authorListNotExpired", authorList);
        model.addAttribute("current", "add-update-authors");
        return "add-update-authors";
    }

    @RequestMapping( value = "/add-update-authors/update", method = RequestMethod.POST )
    public String updateAuthor(HttpServletRequest request){
        Author author = getAuthorFromRequest(request);
        authorService.editAuthor(author);
        return "redirect:/add-update-authors";
    }

    private Author getAuthorFromRequest(HttpServletRequest request) {
        Long authorId = Long.valueOf(request.getParameter("authorId"));
        String authorName = request.getParameter("authorName");
        String expiredParam = request.getParameter("expired");
        Date expired = expiredParam == null || expiredParam.equals("null") || expiredParam.isEmpty()  ? null : new Date(Long.valueOf(expiredParam));
        return new Author(authorId, authorName, expired);
    }

    @RequestMapping( value = "/add-update-authors/add", method = RequestMethod.POST )
    public String addAuthor(@ModelAttribute Author author, BindingResult result){
        authorService.addAuthor(author);
        return "redirect:/add-update-authors";
    }

}
