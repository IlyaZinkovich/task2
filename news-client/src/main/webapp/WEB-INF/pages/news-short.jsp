<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>

<c:forEach items="${newsList}" var="newsItem">
<div>
    <div class="row">
        <div class="col-md-8 column">
            <strong>${newsItem.title}</strong>
        </div>
        <div class="col-md-2 column">
            <p>(<spring:message code='news-short.by' /> ${newsItem.author.name})</p>
        </div>
        <div class="col-md-2 column">
            <c:choose>
                <c:when test="${pageContext.response.locale eq 'ru'}">
                    <fmt:formatDate value="${newsItem.creationDate}" pattern="dd/MM/YYYY" var="formattedDate" />
                </c:when>
                <c:otherwise>
                    <fmt:formatDate value="${newsItem.creationDate}" pattern="MM/dd/YYYY" var="formattedDate" />
                </c:otherwise>
            </c:choose>
            <u class="pull-right">${formattedDate}</u>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12 column">
            <p>${newsItem.shortText}</p>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-8 column">
            <p class="text-muted pull-right">
                <c:forEach items="${newsItem.tags}" var="tagItem">
                    ${tagItem.name}
                </c:forEach>
            </p>
        </div>
        <div class="col-md-2 column">
            <p class="text-warning pull-right">
                <spring:message code='news-short.comments' />(${newsItem.comments.size()})
            </p>
        </div>
        <div class="col-md-2 column">
            <a class="pull-right" href='<spring:url value="/view-news/${newsItem.news.id}.html" />' /><spring:message code='news-short.view' /></a>
        </div>
    </div>
</div>
</c:forEach>
<br>
<br>
<c:if test="${newsList.isEmpty()}">
<h1><spring:message code='news.news-not-found' /><h1>
</c:if>
