<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>
<style>
#comments, #comments li {
    margin: 0;
    padding: 0;
}

#comments li {
    list-style: none;
}
</style>
<script>
    $(document).ready(function() {
        $("#commentForm").validate(
                {
                  rules: {
                    commentText: {
                      required : true
                    }
                  },
                  highlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                  },
                  unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                  },
                  messages: {
                      commentText: "<spring:message code='add-update-news.required-field' />",
                  }
                }
        );
      });
</script>
<div class="row">
    <div class="col-md-12 column">
        <a href='<spring:url value="/news-list.html" />' class="pull-left" /><spring:message code='news-full.back' /></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12 column">
        <h2>
            <b>${news.news.title}</b>
        </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12 column">
        <p class="pull-left">(<spring:message code='news-full.by' /> ${news.author.name})</p>

        <c:choose>
            <c:when test="${pageContext.response.locale eq 'ru'}">
                <fmt:formatDate value="${comment.creationDate}" pattern="dd/MM/YYYY" var="commentDate" />
            </c:when>
            <c:otherwise>
                <fmt:formatDate value="${comment.creationDate}" pattern="MM/dd/YYYY" var="commentDate" />
            </c:otherwise>
        </c:choose>
        <p class="pull-right">${formattedDate}</p>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12 column">
        <h2>
            ${news.news.fullText}
        </h2>
    </div>
</div>
<br>
<div class="container">
    <c:forEach var="comment" items = "${news.comments}">
        <div class="row">
            <div class="col-md-8">
                <ul id="comments" class="comments">
                    <li class="comment">
                        <div class="clearfix" style="background-color:#C0C0C0">
                            <fmt:formatDate value="${comment.creationDate}" pattern="MM/dd/YYYY" var="commentDate" />
                            <p class="pull-left">${commentDate}</p>
                        </div>
                        <p>
                            <em>${comment.commentText}</em>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </c:forEach>

    <form:form method="POST" commandName="commentForm" action="/news-client/view-news/${news.news.id}/add-comment" >
        <div class="row">
            <div class="col-md-8">
                <form:textarea path="commentText" />
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6" />
            <div class="col-md-2">
                <form>
                    <button type="submit" class="btn btn-success green btn-sm"><spring:message code='news-full.post-comment' /></button>
                </form>
            </div>
            <div class="col-md-6" />
        </div>
    </form:form>
</div>

<nav class="navbar navbar-default navbar-fixed-bottom">
<div class="row navbar-inner">
    <div class="col-md-9 col-md-offset-2" >
        <c:if test="${previous != '0'}">
            <a href='<spring:url value="/view-news/${previous}.html" />' class="btn pull-left" /><spring:message code='news-full.previous' /></a>
        </c:if>
        <c:if test="${next != '0'}">
            <a href='<spring:url value="/view-news/${next}.html" />' class="btn pull-right" /><spring:message code='news-full.next' /></a>
        </c:if>
    </div>
</div>

<div class="row navbar-inner">
    <p align="center">
        <spring:message code='footer.copyright' />
    </p>
</div>
</nav>
