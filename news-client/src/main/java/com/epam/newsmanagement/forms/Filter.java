package com.epam.newsmanagement.forms;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

@Component
public class Filter implements Serializable {

    private static final long serialVersionUID = -1431651559395487415L;

    private Long authorId;

    private List<Long> tagsId;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    public boolean isEmpty() {
        return authorId == null && tagsId == null;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "authorId=" + authorId +
                ", tagsId=" + tagsId +
                '}';
    }
}
