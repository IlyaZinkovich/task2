package com.epam.newsmanagement.controller;

class PageConstants {
    public final static Long FIRST_PAGE = 1L;
    public final static Long DEFAULT_PAGE_SIZE = 4L;
}
