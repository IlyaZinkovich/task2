package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.*;
import com.epam.newsmanagement.forms.Filter;
import com.epam.newsmanagement.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import static com.epam.newsmanagement.controller.PageConstants.DEFAULT_PAGE_SIZE;
import static com.epam.newsmanagement.controller.PageConstants.FIRST_PAGE;

@Controller
@SessionAttributes({"filter"})
public class NewsController {

    private static Logger logger = Logger.getLogger(NewsController.class);


    @Autowired
    private NewsService newsService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private NewsManagementService newsManagementService;

    @ModelAttribute("filter")
    public Filter initFilterForm() {
        return new Filter();
    }

    @ModelAttribute("commentForm")
    public Comment initCommentForm() {
        return new Comment();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model, @ModelAttribute Filter filter) {
        return newsListPaged(model, FIRST_PAGE, filter);
    }

    @RequestMapping(value = "/news-list", method = RequestMethod.GET)
    public String newsList(Model model, @ModelAttribute Filter filter) {
        return newsListPaged(model, FIRST_PAGE, filter);
    }

    @RequestMapping(value = "/news-list/{page}", method = RequestMethod.GET)
    public String newsListPaged(Model model, @PathVariable Long page, @ModelAttribute Filter filter) {
        SearchCriteria searchCriteria = filterToSearchCriteria(page, filter);
        List<NewsDTO> newsList = newsManagementService.findNewsDTO(searchCriteria);
        Long newsCount = newsService.getNewsCount(searchCriteria);
        List<Author> authorList = authorService.findHavingNews();
        List<Tag> tagList = tagService.findAll();
        Long pageCount = (long) Math.ceil((double) newsCount / DEFAULT_PAGE_SIZE);
        model.addAttribute("newsList", newsList);
        model.addAttribute("tagList", tagList);
        model.addAttribute("authorList", authorList);
        model.addAttribute("pageCount", pageCount);
        model.addAttribute("filter", filter);
        model.addAttribute("current", "news-list");
        return "news-list";
    }

    private SearchCriteria filterToSearchCriteria(Long page, Filter filter) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthorId(filter.getAuthorId());
        searchCriteria.setTagsId(filter.getTagsId());
        searchCriteria.setPage(page);
        searchCriteria.setPageSize(PageConstants.DEFAULT_PAGE_SIZE);
        return searchCriteria;
    }

    @RequestMapping(value = "/view-news/{id}", method = RequestMethod.GET)
    public String viewNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        NewsDTO newsDTO = newsManagementService.findNewsDTOById(id);
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        addPreviousAndNextNewsIdToTheModel(model, previousAndNextNews);
        model.addAttribute("news", newsDTO);
        return "view-news";
    }

    private void addPreviousAndNextNewsIdToTheModel(Model model, List<Long> previousAndNextNews) {
        if (previousAndNextNews.isEmpty()) return;
        if (previousAndNextNews.get(0) != null) {
            model.addAttribute("previous", previousAndNextNews.get(0));
        }
        if (previousAndNextNews.get(1) != null) {
            model.addAttribute("next", previousAndNextNews.get(1));
        }
    }

    @RequestMapping(value = "/view-news/{newsId}/add-comment", method = RequestMethod.POST)
    public String addComment(@PathVariable Long newsId, Model model, @ModelAttribute Comment comment) {
        if (comment.getCommentText() == null) {
            return "redirect:/view-news/" + newsId;
        }
        comment.setCreationDate(new Date());
        comment.setNewsId(newsId);
        commentService.addComment(comment);
        NewsDTO newsDTO = newsManagementService.findNewsDTOById(newsId);
        model.addAttribute("news", newsDTO);
        return "redirect:/view-news/" + newsId;
    }

    @RequestMapping(value = "/view-news/{id}/previous")
    public String previousNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        NewsDTO newsDTO = newsManagementService.findNewsDTOById(previousAndNextNews.get(0));
        model.addAttribute("news", newsDTO);
        return "redirect:/view-news/" + previousAndNextNews.get(0);
    }

    @RequestMapping(value = "/view-news/{id}/next")
    public String nextNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        NewsDTO newsDTO = newsManagementService.findNewsDTOById(previousAndNextNews.get(1));
        model.addAttribute("news", newsDTO);
        return "redirect:/view-news/" + previousAndNextNews.get(1);
    }

    @RequestMapping(value = "/news-list/filter", method = RequestMethod.POST)
    public String filterNews(Model model, @ModelAttribute Filter filter, BindingResult result) {
        return newsListPaged(model, FIRST_PAGE, filter);
    }

    @RequestMapping(value = "/news-list/filter", method = RequestMethod.GET)
    public String showFilteredNews() {
        return "redirect:/news-list";
    }


    @RequestMapping(value = "/news-list/filter/reset", method = RequestMethod.GET)
    public String showNews(Model model) {
        return newsListPaged(model, FIRST_PAGE, new Filter());
    }
}
