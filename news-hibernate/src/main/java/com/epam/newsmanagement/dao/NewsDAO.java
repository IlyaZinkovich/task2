package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;

import java.util.List;

/**
 * A DatabaseAccessObject interface that
 * provides access to news data in the data source.
 */
public interface NewsDAO extends GenericDAO<News> {

    /**
     * Creates a link between the news and its author.
     *
     * @param  newsId
     *         The news id
     *
     * @param  authorId
     *         The id of news author
     *
     */
    void insertNewsAuthor(Long newsId, Long authorId);

    /**
     * Creates a link between the news and its tag.
     *
     * @param  newsId
     *         The news id
     *
     * @param  tagId
     *         The id of news tag
     *
     */
    void insertNewsTag(Long newsId, Long tagId);

    /**
     * Creates a link between the news and its tags.
     *
     * @param  newsId
     *         The news id
     *
     * @param  tagIdList
     *         The list of news tag id's
     *
     */
    void insertNewsTags(Long newsId, List<Long> tagIdList);

    /**
     * Deletes reference on news with the specified id
     * from the news-tag join table.
     *
     * @param  newsId
     *         The id of news which references to delete
     *
     */
    void deleteNewsTags(Long newsId);

    /**
     * Deletes comments referred on news with the specified id
     * from the comments table.
     *
     * @param  newsId
     *         The id of news which references to delete
     *
     */
    void deleteNewsComments(Long newsId);

    /**
     * Deletes reference on news with the specified id
     * from the news-author join table.
     *
     * @param  newsId
     *         The id of news which references to delete
     *
     */
    void deleteNewsAuthor(Long newsId);


    /**
     * Returns the news id list of news found by search criteria.
     *
     * @param searchCriteria
     *          the criteria that are used to find news
     *
     * @return  the List of news id
     *
     */
    List<News> findNews(SearchCriteria searchCriteria);

    /**
     * Returns the news id list of two news in the filtered by search criteria list
     * previous and next by one to the news with the given id.
     *
     * @param searchCriteria
     *          the criteria that are used to find news
     *
     * @return  the List of news id,
     *          the first element is 0 if the news with the given id is the first in the whole filtered news list
     *          the second element is 0 if the news with the given id is the last in the whole filtered news list
     *
     */
    List<Long> findPreviousAndNextNews(Long newsId, SearchCriteria searchCriteria);

    /**
     * Returns the count of news found by search criteria.
     *
     * @param searchCriteria
     *          the criteria that are used to find news
     *
     * @return  the integer count of news
     *
     */
    Long getNewsCount(SearchCriteria searchCriteria);

}
