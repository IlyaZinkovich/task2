package com.epam.newsmanagement.dao.impl;


import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.Tag;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.List;

public class CommentDAOImpl implements CommentDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Long insert(Comment comment) {
        return (Long) this.sessionFactory.getCurrentSession().save(comment);
    }

    @Override
    public void update(Comment comment) {
        this.sessionFactory.getCurrentSession().save(comment);
    }

    @Override
    public void delete(Long commentId) {
        Comment comment = (Comment) sessionFactory.getCurrentSession().load(
                Tag.class, commentId);
        if (null != comment) {
            this.sessionFactory.getCurrentSession().delete(comment);
        }
    }

    @Override
    public List<Comment> findAll() {
        return this.sessionFactory.getCurrentSession().createCriteria(Comment.class).list();
    }

    @Override
    public Comment findById(Long commentId) {
        return (Comment) this.sessionFactory.getCurrentSession().get(Comment.class, commentId);
    }

    @Override
    public List<Long> insert(List<Comment> comments) {
        List<Long> idList = new LinkedList<>();
        for (Comment c : comments) {
            Long id = (Long) this.sessionFactory.getCurrentSession().save(c);
            idList.add(id);
        }
        return idList;
    }

    @Override
    public List<Comment> findByNewsId(Long newsId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select a from Comment a inner join News n " +
                " where n.news_id=" + newsId);
        return query.list();
    }
}
