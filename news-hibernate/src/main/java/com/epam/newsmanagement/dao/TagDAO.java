package com.epam.newsmanagement.dao;


import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Tag;

import java.util.List;

/**
 * A DatabaseAccessObject interface that
 * provides access to tags data in the data source.
 */
public interface TagDAO extends GenericDAO<Tag> {

    /**
     * Inserts the given tags into a data source
     * and returns the list of generated id's.
     *
     * @param  tags
     *         The list of tags
     *
     * @return  the list of generated id's
     *
     */
    List<Long> insert(List<Tag> tags);

    /**
     * Returns the list of tags
     * that news with the given id has.
     *
     * @param  newsId
     *         The news id
     *
     * @return  the list of tags
     * that news with the given id has
     *
     */
    List<Tag> findByNewsId(Long newsId);

    /**
     * Deletes reference on tag with the specified id
     * from the news-tag join table.
     *
     * @param  tagId
     *         The id of tag which references to delete
     *
     */
    void deleteNewsTag(Long tagId);

}
