package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.NewsAuthor;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class AuthorDAOImpl implements AuthorDAO {

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Author findByNewsId(Long newsId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select a from Author a " +
                " inner join a.news n " +
                " where n.id=:newsId");
        query.setParameter("newsId", newsId);
        List authorList = query.list();
        return authorList.isEmpty() ? null : (Author)authorList.get(0);
    }

    @Override
    public List<Author> findNotExpired() {
        return this.sessionFactory.getCurrentSession().createQuery("select a from Author a where a.expired is null").list();
    }

    @Override
    public List<Author> findHavingNews() {
        Query query = this.sessionFactory.getCurrentSession().createQuery("select a from Author a inner join a.news n ");
        return query.list();
    }

    @Override
    public void deleteNewsAuthor(Long authorId) {
    }

    @Override
    public void update(Long authorId, Date expirationDate) {
        Author author = findById(authorId);
        author.setExpired(expirationDate);
        update(author);
    }

    @Override
    public Long insert(Author item) {
        Author author = (Author) this.sessionFactory.getCurrentSession().get(Author.class, item.getId());
        if (author != null) {
            return author.getId();
        }
        return (Long) this.sessionFactory.getCurrentSession().save(item);
    }

    @Override
    public void update(Author item) {
        sessionFactory.getCurrentSession().saveOrUpdate(item);
    }

    @Override
    public void delete(Long itemId) {
        Author author = (Author) sessionFactory.getCurrentSession().load(
                Author.class, itemId);
        if (null != author) {
            this.sessionFactory.getCurrentSession().delete(author);
        }
    }

    @Override
    public List<Author> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Author").list();
        //return this.sessionFactory.getCurrentSession().createCriteria(Author.class).list();
    }

    @Override
    public Author findById(Long id) {
        return (Author) this.sessionFactory.getCurrentSession().get(Author.class, id);
    }
}