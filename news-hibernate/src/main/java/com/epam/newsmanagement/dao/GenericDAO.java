package com.epam.newsmanagement.dao;


import com.epam.newsmanagement.dao.exception.DAOException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(propagation = Propagation.REQUIRED)
public interface GenericDAO<T> {
    /**
     * Inserts the given item into a data source
     * and returns the generated id.
     *
     * @param  item
     *         The item to be inserted
     *
     * @return  the generated id
     *
     */
    Long insert(T item);

    /**
     * Updates the information about the given item in the data source.
     *
     * @param  item
     *         The item to be inserted
     *
     */
    void update(T item);

    /**
     * Deletes the {@code T} object with the given id from the data source.
     *
     * @param  itemId
     *         The id of item to be deleted
     *
     */
    void delete(Long itemId);

    /**
     * Returns the list of all the object of {@code T} type found in the data source.
     *
     * @return  the list of all the object of {@code T} type found in the data source
     *
     */
    List<T> findAll();

    /**
     * Returns the {@code T} object with the given id found in the data source.
     *
     * @param  id
     *         The id of {@code T} object
     *
     * @return  the {@code T} object with the given id found in the data source
     *
     */
    T findById(Long id);
}
