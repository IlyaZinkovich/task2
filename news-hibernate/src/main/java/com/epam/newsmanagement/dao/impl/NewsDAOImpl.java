package com.epam.newsmanagement.dao.impl;


import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.*;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class NewsDAOImpl implements NewsDAO {

    private static Logger logger = Logger.getLogger(NewsDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void insertNewsAuthor(Long newsId, Long authorId) {
        Author author = (Author) sessionFactory.getCurrentSession().get(Author.class, authorId);
        News news = (News) sessionFactory.getCurrentSession().get(News.class, newsId);
        if (author != null && news != null) {
            this.sessionFactory.getCurrentSession().save(new NewsAuthor(news, author));
        }
    }

    @Override
    public void insertNewsTag(Long newsId, Long tagId) {
        Tag tag = (Tag) sessionFactory.getCurrentSession().get(Author.class, tagId);
        News news = (News) sessionFactory.getCurrentSession().get(News.class, newsId);
        if (tag != null && news != null) {
            this.sessionFactory.getCurrentSession().save(new NewsTag(news, tag));
        }
    }


    @Override
    public void insertNewsTags(Long newsId, List<Long> tagIdList) {
        for (Long tagId : tagIdList) insertNewsTag(newsId, tagId);
    }

    @Override
    public News findById(Long newsId) {
        return (News) this.sessionFactory.getCurrentSession().get(News.class, newsId);
    }

    @Override
    public Long insert(News news) {
        return (Long) this.sessionFactory.getCurrentSession().save(news);
    }

    @Override
    public void update(News news) {
        this.sessionFactory.getCurrentSession().save(news);
    }

    @Override
    public void delete(Long newsId) {
        News news = (News) this.sessionFactory.getCurrentSession().get(News.class, newsId);
        if (news != null) {
            this.sessionFactory.getCurrentSession().delete(news);
        }
    }

    @Override
    public void deleteNewsTags(Long newsId) {

    }

    @Override
    public void deleteNewsComments(Long newsId) {

    }

    @Override
    public void deleteNewsAuthor(Long newsId) {

    }

    @Override
    public List<Long> findPreviousAndNextNews(Long newsId, SearchCriteria searchCriteria)  {
        return null;
    }

    @Override
    public List<News> findNews(SearchCriteria searchCriteria) {
        return this.sessionFactory.getCurrentSession().createCriteria(News.class).
                add(Restrictions.eq("author_id", searchCriteria.getAuthorId())).
                add(Restrictions.eq("tags_id", searchCriteria.getTagsId())).
                setFirstResult((searchCriteria.getPage().intValue() * searchCriteria.getPageSize().intValue())).setMaxResults(searchCriteria.getPageSize().intValue()).list();

    }

    @Override
    public Long getNewsCount(SearchCriteria searchCriteria) {
        return Long.valueOf(this.findNews(searchCriteria).size());
    }


    @Override
    public List<News> findAll() {
        throw new UnsupportedOperationException();
    }

}