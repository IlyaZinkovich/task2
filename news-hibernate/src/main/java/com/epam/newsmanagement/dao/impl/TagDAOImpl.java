package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.List;

public class TagDAOImpl implements TagDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Long> insert(List<Tag> tags) {
        List<Long> idList = new LinkedList<>();
        for (Tag t : tags) {
            Long id = (Long) this.sessionFactory.getCurrentSession().save(t);
            idList.add(id);
        }
        return idList;
    }

    @Override
    public Tag findById(Long tagId) {
        return (Tag) this.sessionFactory.getCurrentSession().get(Tag.class, tagId);
    }

    @Override
    public Long insert(Tag tag) {
        return (Long) this.sessionFactory.getCurrentSession().save(tag);
    }

    @Override
    public void update(Tag tag) {
        this.sessionFactory.getCurrentSession().save(tag);
    }

    @Override
    public void delete(Long tagId) {
        Tag tag = (Tag) sessionFactory.getCurrentSession().load(
                Tag.class, tagId);
        if (null != tag) {
            this.sessionFactory.getCurrentSession().delete(tag);
        }
    }

    @Override
    public List<Tag> findAll() {
        return this.sessionFactory.getCurrentSession().createCriteria(Tag.class).list();
    }

    @Override
    public List<Tag> findByNewsId(Long newsId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select a from Tag a inner join News n " +
                " where n.news_id=" + newsId);
        return query.list();
    }

    @Override
    public void deleteNewsTag(Long tagId) {
        delete(tagId);
    }

}