package com.epam.newsmanagement.domain;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Embeddable
@Table(name = "news_tag")
public class NewsTag implements java.io.Serializable {

    private static final long serialVersionUID = -9120607274421816301L;
    private News news;
    private Tag tag;

    public NewsTag(News news, Tag tag) {
        this.news = news;
        this.tag = tag;
    }

    @ManyToOne
    public Tag getTag() {
        return tag;
    }

    @ManyToOne
    public News getNews() {
        return news;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public void setNews(News news) {
        this.news = news;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o.getClass() != this.getClass()) return false;

        NewsTag newsTag = (NewsTag) o;

        if (tag != null ? !tag.equals(newsTag.tag) : newsTag.tag != null) return false;
        return !(news != null ? !news.equals(newsTag.news) : newsTag.news != null);

    }

    @Override
    public int hashCode() {
        int result = tag != null ? tag.hashCode() : 0;
        result = 31 * result + (news != null ? news.hashCode() : 0);
        return result;
    }
}
