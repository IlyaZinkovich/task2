package com.epam.newsmanagement.domain;


import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "comments")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1835364775299837961L;

    @Id
    @GeneratedValue
    @Column(name = "comment_id")
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "comment_text")
    private String commentText;

    @NotNull
    @Type(type = "timestamp")
    @Column(name = "creation_date")
    private Date creationDate;

    @Transient
    private Long newsId;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "news_id")
    private News news;

    public Comment() {
    }

    public Comment(Long id, String commentText, Date creationDate, Long newsId) {
        this.id = id;
        this.commentText = commentText;
        this.creationDate = creationDate;
        this.newsId = newsId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public void setNews(News news) {
        this.news = news;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o != null && o.getClass() != this.getClass()) return false;

        Comment comment = (Comment) o;

        if (id != comment.id) return false;
        if (!Objects.equals(newsId, comment.newsId)) return false;
        if (!commentText.equals(comment.commentText)) return false;
        if (!creationDate.equals(comment.creationDate)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + commentText.hashCode();
        result = 31 * result + creationDate.hashCode();
        result = 31 * result + (int) (newsId ^ (newsId >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", commentText='" + commentText + '\'' +
                ", creationDate=" + creationDate +
                ", newsId=" + newsId +
                '}';
    }
}
