package com.epam.newsmanagement.domain;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Embeddable
@Table(name = "news_author")
public class NewsAuthor implements java.io.Serializable {

    private static final long serialVersionUID = -9120607274421816301L;

    private News news;
    private Author author;

    public NewsAuthor(News news, Author author) {
        this.news = news;
        this.author = author;
    }

    @ManyToOne
    public Author getAuthor() {
        return author;
    }

    @ManyToOne
    public News getNews() {
        return news;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public void setNews(News news) {
        this.news = news;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o.getClass() != this.getClass()) return false;

        NewsAuthor that = (NewsAuthor) o;

        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        return !(news != null ? !news.equals(that.news) : that.news != null);

    }

    @Override
    public int hashCode() {
        int result = author != null ? author.hashCode() : 0;
        result = 31 * result + (news != null ? news.hashCode() : 0);
        return result;
    }
}
