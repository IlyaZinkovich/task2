package com.epam.newsmanagement.dao.impl;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.*;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:spring-context.xml")
@TransactionConfiguration(defaultRollback=true, transactionManager="transactionManager")
public class AuthorDAOTest {

	private static Logger logger = Logger.getLogger(AuthorDAOTest.class);

	@Autowired
	private AuthorDAO authorDAO;

	private int notExpiredAuthorRange = 10;

	private Author testAuthor;

	@Test
	public void findAuthorByNewsId() throws Exception {
		authorDAO.findByNewsId(1L);
	}

	@Before
	public void setUp() {
		testAuthor = new Author(100L, "Bill", null);
	}
//
//
//	@Test
//	public void findAll() throws Exception {
//		List<Author> authorList = authorDAO.findAll();
//		assertNotNull(authorList.size());
//	}
//
//	@Test
//	public void findById() throws Exception {
//		Long id = 1L;
//		Author author = authorDAO.findById(id);
//		assertEquals(author.getId(), id);
//	}
//
//	@Test
//	public void findNotExpired() throws Exception {
//		List<Author> authorList = authorDAO.findNotExpired();
//		assertNotNull(authorList.size());
//	}
//
//	@Test
//	public void insertAuthorSucceed() throws Exception {
//		List<Author> authorsBefore = authorDAO.findAll();
//		long generatedId = authorDAO.insert(testAuthor);
//		assertThat(generatedId, greaterThan(0l));
//		testAuthor.setId(generatedId);
//		List<Author> authorsAfter = authorDAO.findAll();
//		assertThat(authorsAfter.size(), is(authorsBefore.size() + 1));
//	}
//
//	@Test
//	public void insertAuthorDoesNothingIfAuthorWithThisNameAlreadyExists() throws Exception {
//		List<Author> foundAuthorsBefore = authorDAO.findAll();
//		Author authorToInsert = foundAuthorsBefore.get(0);
//		long generatedId = authorDAO.insert(authorToInsert);
//		assertThat(generatedId, is(authorToInsert.getId()));
//		List<Author> foundAuthorsAfter = authorDAO.findAll();
//		assertThat(foundAuthorsBefore, is(foundAuthorsAfter));
//	}
//
//	@Test
//	public void updateAuthorSucceed() throws Exception {
//		List<Author> foundAuthorsBefore = authorDAO.findAll();
//		Author authorToUpdate = foundAuthorsBefore.get(0);
//		authorToUpdate.setExpired(new Date());
//		authorToUpdate.setName(testAuthor.getName());
//		authorDAO.update(authorToUpdate);
//		Author updatedAuthor = authorDAO.findById(authorToUpdate.getId());
//		assertEquals(authorToUpdate, updatedAuthor);
//	}
//
//	@Test
//	public void deleteAuthorSucceed() throws Exception {
//		List<Author> foundAuthorsBefore = authorDAO.findAll();
//		Author authorToDelete = foundAuthorsBefore.get(0);
//		authorDAO.deleteNewsAuthor(authorToDelete.getId());
//		authorDAO.delete(authorToDelete.getId());
//		Author deletedAuthor = authorDAO.findById(authorToDelete.getId());
//		assertThat(deletedAuthor, nullValue());
//	}
//
//	@Test
//	public void findByNewsIdSucceed() throws Exception {
//		Author foundAuthor = authorDAO.findByNewsId(5L);
//		assertNotNull(foundAuthor);
//	}


}
